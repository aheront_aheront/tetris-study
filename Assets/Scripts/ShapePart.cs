using UnityEngine;

public class ShapePart : MonoBehaviour
{
    // ���������� ������ (X, Y)
    public Vector2Int CellId;

    // ������������� ������� ������
    public void SetPosition(Vector2 position)
    {
        // ������ ������� ������ ��������
        transform.position = position;
    }

    public bool GetActive() => gameObject.activeSelf;
    public void SetActive(bool a) => gameObject.SetActive(a);

}