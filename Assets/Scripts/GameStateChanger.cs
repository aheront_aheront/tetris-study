using TMPro;
using UnityEngine;

public class GameStateChanger : MonoBehaviour
{
    // ������ �������� ����
    public GameField GameField;

    // ������ �������� �����
    public ShapeMover ShapeMover;
    // ������ ��������� �����
    public ShapeSpawner ShapeSpawner;
    // ����� ���� 
    public GameObject GameScreen;

    // ����� ����� ����
    public GameObject GameEndScreen;

    private void Start()
    {
        // �������� ����� FirstStartGame();
        FirstStartGame();
    }

    private void FirstStartGame()
    {
        GameField.FillCellsPositions();

        // �����: �������� ����� ������ ����
        StartGame();
    }

    public void SpawnNextShape()
    {
        // ������ ���������� nextShape, � ������� ���������� ��������� ������, ��������������� ShapeSpawner
        Shape nextShape = ShapeSpawner.SpawnNextShape();

        // ������������� ��������� ������ � ShapeMover, ������� �������� �� ����������� �����
        ShapeMover.SetTargetShape(nextShape);

        // �������� ������ � �������� ������� �� ������� ����
        ShapeMover.MoveShape(Vector2Int.right * (int)(GameField.FieldSize.x * 0.5f) + Vector2Int.up * (GameField.FieldSize.y - GameField.InvisibleYFieldSize + nextShape.ExtraSpawnYMove));
    
    }

    private void StartGame()
    {
        // ���������� ����� ������
        SpawnNextShape();

        // ������������� ����� ����
        SwitchScreens(true);

        // �������� �������� �����
        ShapeMover.SetActive(true);
    }

    public void EndGame()
    {
        // �����: ������������� ���� � ����� ����
        RefreshScores();

        SwitchScreens(false);
        ShapeMover.SetActive(false);
    }

    private void SwitchScreens(bool isGame)
    {
        // ���������� ����� ����
        GameScreen.SetActive(isGame);

        // �������� ����� ���������� ����
        GameEndScreen.SetActive(!isGame);
    }

    public void RestartGame()
    {
        // �����: �������� ���� ��� ����������� ����
        Score.Restart();

        ShapeMover.DestroyAllShapes();
        StartGame();
    }

    // ������ ������� �����
    public Score Score;

    // ������� � ����� ����
    public TextMeshProUGUI GameEndScoreText;

    // ������� � ������� ������
    public TextMeshProUGUI BestScoreText;
    private void RefreshScores()
    {
        // �������� ������� ����
        int score = Score.GetScore();

        // �������� ������� ������ ����
        int oldBestScore = Score.GetBestScore();

        // ���������, ����� �� ����� ������
        bool isNewBestScore = CheckNewBestScore(score, oldBestScore);

        // � ����������� �� ���������� ���������� ��� �������� ����� ��� ����� ������
        SetActiveGameEndScoreText(!isNewBestScore);

        // ���� ����� ����� ������
        if (isNewBestScore)
        {
            // ������������� ����� ������ ����
            Score.SetBestScore(score);

            // ����� ����� � ����� �������
            SetNewBestScoreText(score);
        }
        // �����
        else
        {
            // ������������� ����� � ������� �����
            SetGameEndScoreText(score);

            // ����� ����� � ������� �������
            SetOldBestScoreText(oldBestScore);
        }
    }
    private bool CheckNewBestScore(int score, int oldBestScore)
    {
        // ���������� ��������� �������� ����, ��� ������� ���� ���� ������� (true ��� false)
        return score > oldBestScore;
    }

    private void SetGameEndScoreText(int value)
    {
        // ��������� ������� ����� ����
        GameEndScoreText.text = $"���� ��������!\n���������� �����: {value}";
    }

    private void SetOldBestScoreText(int value)
    {
        // ��������� ������� ������� �����
        BestScoreText.text = $"������ ���������: {value}";
    }

    private void SetNewBestScoreText(int value)
    {
        // ��������� ������� ������ �������
        BestScoreText.text = $"����� ������: {value}!";
    }

    private void SetActiveGameEndScoreText(bool value)
    {
        // ������������� ���������� ���������� ���� ����� � ����� ���� � ����������� �� �������� value
        GameEndScoreText.gameObject.SetActive(value);
    }
}